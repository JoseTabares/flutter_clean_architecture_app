import 'package:domain/secutiry_use_case.dart';

mixin SecurityApi {}
mixin SecurityDb {}

class SecurityRepositoryAdapter implements SecurityRepository {
  final SecurityApi api;
  final SecurityDb db;

  SecurityRepositoryAdapter(
    this.api,
    this.db,
  );
}
