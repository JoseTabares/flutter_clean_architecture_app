mixin MyConnectivity {
  Future<bool> isConnected();
}
