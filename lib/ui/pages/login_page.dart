import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture_app/blocs/login_bloc.dart';
import 'package:flutter_clean_architecture_app/di/injector.dart';

import 'base_state.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends BaseState<LoginPage, LoginBloc> {
  @override
  LoginBloc getBlocInstance() {
    return LoginBloc(Injector().provideSecurityUseCase());
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
