import 'package:domain/secutiry_use_case.dart';
import 'package:flutter_clean_architecture_app/di/repository_injector.dart';

class Injector {
  static Injector _singleton;

  factory Injector() {
    if (_singleton == null) {
      _singleton = Injector._();
    }
    return _singleton;
  }

  Injector._();

  SecurityUseCase provideSecurityUseCase() {
    return SecurityUseCaseAdapter(
      RepositoryInjector().provideSecurityRepository(),
    );
  }
}
