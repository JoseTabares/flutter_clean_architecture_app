mixin SecurityRepository {}

mixin SecurityUseCase {}

class SecurityUseCaseAdapter implements SecurityUseCase {
  final SecurityRepository repository;

  SecurityUseCaseAdapter(this.repository);
}
